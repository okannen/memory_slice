#![allow(dead_code)]
#![allow(unused_variables)]
extern crate memory_slice;
use memory_slice::DefaultAt;
use std::marker::PhantomData;
use std::cell::UnsafeCell;

#[derive(DefaultAt)]
#[repr(C)]
struct X <T:Copy=u64>where T:Eq {
    #[default_init_value(10)]
    a: u32,
    b: u64,
    phantom: PhantomData<T>,
}
#[repr(C)]
#[derive(DefaultAt)]
struct Y(u32, #[default_init_value(42)] u64);

#[repr(C)]
#[derive(DefaultAt)]
struct Z;

//impl DefaultAt for X {
//    fn default_moving_to(mem: &Memory) -> Self {
//        X {
//            a: <u32 as DefaultAt>::default_moving_to(&mem[span_of!(Self, a)]),
//            b: <u64 as DefaultAt>::default_moving_to(&mem[span_of!(Self, a)]),
//        }
//    }
//}
#[repr(C)]
#[derive(DefaultAt)]
struct A(X, Y,PhantomData<u32>);

//Extremely dangerous, use must be sure
//that UnsafeCell and its inner field have
//the exact same address! (Which is the case
//actualy as UnsafeCell is repr(transparent))
#[derive(DefaultAt)]
#[repr(C)]
struct B {
    #[default_init_with(UnsafeCell::new(field))]
    a: UnsafeCell<Y>,
}

#[cfg(test)]
mod test {
    use super::{X, Y,Z, A, B};
    use std::pin::Pin;

    use memory_slice::BufferAs;
    use memory_slice::DefaultAt;
    #[test]
    fn default_at() {
        let mut b = BufferAs::<X>::new();
        let v: Pin<&mut X> = X::default_at(&mut *b);
        assert_eq!(v.a, 10);
        assert_eq!(v.b, 0);
        let mut b = BufferAs::<Y>::new();
        let v: Pin<&mut Y> = Y::default_at(&mut *b);
        assert_eq!(v.0, 0);
        assert_eq!(v.1, 42);

        let mut b = BufferAs::<Z>::new();
        let v: Pin<&mut Z> = Z::default_at(&mut *b);

        let mut b = BufferAs::<A>::new();
        let v: Pin<&mut A> = A::default_at(&mut *b);
        assert_eq!(v.0.a, 10);
        assert_eq!(v.0.b, 0);
        assert_eq!(v.1.0, 0);
        assert_eq!(v.1.1, 42);

        let mut b = BufferAs::<B>::new();
        let v: Pin<&mut B> = B::default_at(&mut *b);
        assert_eq!(unsafe{(&*v.a.get()).0}, 0);
        assert_eq!(unsafe{(&*v.a.get()).1}, 42);
    }
}
