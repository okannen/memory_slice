extern crate proc_macro;
use std::vec::Vec;
use syn::{parse_macro_input};

extern crate quote;
use quote::{quote, quote_spanned};

extern crate proc_macro2;
use proc_macro2::{Span, TokenStream};

#[proc_macro_derive(DefaultAt, attributes(default_init_value,default_init_with))]
pub fn default_at(item: proc_macro::TokenStream) -> proc_macro::TokenStream {
    use syn::*;
    let ast: ItemStruct = parse_macro_input!(item);
    let (impl_generics, ty_generics, where_clause) = ast.generics.split_for_impl();
    let ty = ast.ident;
    match ast.fields {
        Fields::Named(fs) => {
            let mut initer = vec![];
            for f in fs.named {
                let id = f.ident;
                let expr = get_init_expression(f.attrs,
                    quote!(
                    unsafe {::memory_slice::DefaultAt::default_moving_to(mem.get_unchecked(::memory_slice::span_of!(Self,#id)))}
             ).into()
                );

                initer.push(FieldValue {
                    attrs: vec![],
                    member: Member::Named(id.clone().unwrap()),
                    colon_token: Some(token::Colon {
                        spans: [Span::call_site()],
                    }),
                    expr: syn::parse2(expr).unwrap(),
                });
            }
            quote!(
            impl #impl_generics ::memory_slice::DefaultAt for #ty #ty_generics #where_clause {
                unsafe fn default_moving_to(mem: &::memory_slice::Memory) -> Self {
                    Self {
                     #(
                       #initer
                       ),*
                    }
                }
            }
            )
            .into()
        }
        Fields::Unnamed(fs) => {
            let mut initer = vec![];
            for (i, f) in fs.unnamed.into_iter().enumerate() {
                let id = LitInt::new(i.to_string().as_str(), Span::call_site());
                initer.push( get_init_expression(f.attrs,quote!(
                    unsafe{::memory_slice::DefaultAt::default_moving_to(mem.get_unchecked(::memory_slice::span_of!(Self,#id)))}
             ).into()));
            }
            quote!(
            impl #impl_generics ::memory_slice::DefaultAt for #ty #ty_generics #where_clause {
                unsafe fn default_moving_to(mem: &::memory_slice::Memory) -> Self {
                    #[allow(unused_parens)]
                    Self (
                     #(
                       #initer
                       ),*
                    )
                }
            }
            )
            .into()
        }
        Fields::Unit => quote!(
        impl #impl_generics ::memory_slice::DefaultAt for #ty #ty_generics #where_clause {
            unsafe fn default_moving_to(mem: &::memory_slice::Memory) -> Self {
                Self
            }
        }
        )
        .into(),
    }
}

fn get_init_expression(v: Vec<syn::Attribute>,inner: TokenStream) -> TokenStream {
    for at in v {
        if at.path.is_ident("default_init_value") {
            if at.tokens.is_empty() {
                return quote_spanned!(at.bracket_token.span=> compile_error!("Expected initializer expression after 'default_init_with' attribute (eg: `#[default_init_with(10+3)]`)")).into();
            } else {
                return at.tokens;
            }
        } else if at.path.is_ident("default_init_with") {
            if at.tokens.is_empty() {
                return quote_spanned!(at.bracket_token.span=> compile_error!("Expected initializer expression after 'default_init_with' attribute (eg: `#[default_init_with(10+3)]`)")).into();
            } else {
                let toks = at.tokens;
                return quote_spanned!(at.bracket_token.span=>
                    {let field = #inner; #[allow(unused_parens)] #toks}
                ).into();
            }
        }
    }
    inner
}

//fn default_at2(mut item: TokenStream) -> TokenStream {
//    use syn::*;
//    let input: ItemStruct = parse2(item.clone()).expect("err I");
//    let id = input.ident;
//    ItemImpl {
//        attrs:vec![],
//        defaultness:None,
//        unsafety:None,
//        impl_token: token::Impl{span:Span::call_site()},
//        generics:input.generics.clone(),
//        trait_:Some((None,parse_quote!(::memory_slice::DefaultAt),token::For{span:Span::call_site()})),
//        self_ty:parse2(quote!("X").into()).expect("err II"),
//        brace_token: token::Brace{span:Span::call_site()},
//        items:vec![]}.to_tokens(&mut item);
//    item
//    //use syn::Fields::*;
//    //match input.fields {
//    //    Named(fs) => for_named(fs),
//    //    Unnamed(fs) => for_unnamed(fs),
//    //    Unit => for_unit()
//    //}
//}
//fn for_named(fs: syn::FieldsNamed) -> TokenStream
//    {
//    for f in fs.named {
//        f.ident.unwrap().to_tokens()
//    }
//    todo!()
//    }
//fn for_unnamed(fs: syn::FieldsUnnamed) -> TokenStream
//    {
//    todo!()
//    }
//fn for_unit() -> TokenStream
//{
//todo!()
//}

//#[proc_macro]
//pub fn cstr_iter(item: TokenStream) -> TokenStream {
//    parse_for(item, make_cstr_iter)
//}
//
//#[cfg(feature="sized_cstr")]
//#[proc_macro]
//pub fn sized_cstr(item: TokenStream) -> TokenStream {
//    parse_for(item, make_sized_cstr)
//}
//
//fn parse_for(item: TokenStream, conv: impl FnOnce(Vec<u8>, Span) -> TokenStream) -> TokenStream {
//    let input = parse_macro_input!(item as Lit);
//    match input {
//        Lit::Str(l) => from_bytes(l.value().into(), l.span(), conv),
//        Lit::ByteStr(l) => from_bytes(l.value(),l.span(), conv),
//        _ => quote!(compile_error!("Expected a string literal or byte string literal.")).into(),
//    }
//}
//
//fn from_bytes(mut v: Vec<u8>, span: Span, conv: impl FnOnce(Vec<u8>,Span) -> TokenStream) -> TokenStream {
//    match memchr(0,&v) {
//        Some(_) => return quote_spanned!(span=>compile_error!("cstrings should not contain inner null byte.")).into(),
//        _ => ()
//    }
//    v.push(0);
//    conv(v, span)
//}
//
//fn make_cstr(v: Vec<u8>, span: Span) -> TokenStream {
//    let v = LitByteStr::new(&v, span);
//    quote_spanned!(span=>
//    unsafe{::cstring_core::CStr::from_bytes_unchecked(#v)}
//    ).into()
//}
//fn make_cstr_iter(v: Vec<u8>, span: Span) -> TokenStream {
//    let v = LitByteStr::new(&v, span);
//    quote_spanned!(span=>
//    unsafe{<::cstring_core::CStrIter<'static>>::from_ptr_unchecked(#v.as_ptr())}
//    ).into()
//}
//
//#[cfg(feature="sized_cstr")]
//fn make_sized_cstr(v: Vec<u8>,span: Span) -> TokenStream {
//    quote_spanned!(span=>
//    unsafe{::cstring_core::CStr::new_unchecked([#(#v),*])}
//    ).into()
//}
//
//#[proc_macro]
//pub fn c_char(item: TokenStream) -> TokenStream {
//    let input = parse_macro_input!(item as Lit);
//    match input {
//        Lit::Char(l) => {
//            let mut csu = [0;4];
//            let csu = l.value().encode_utf8(&mut csu);
//            if csu.len() > 1 {
//                quote!(compile_error!("The char shall be a one byte utf8 encoded character")).into()
//            } else {
//                from_byte(csu.as_bytes()[0], l.span())
//            }
//        },
//        Lit::Byte(l) => from_byte(l.value(),l.span()),
//        _ => quote!(compile_error!("Expected a byte or char literal.")).into(),
//    }
//}
//fn from_byte(v: u8, span: Span) -> TokenStream {
//    if v == 0 {
//        quote_spanned!(span=>compile_error!("c_char shall not be null.")).into()
//    } else {
//        quote_spanned!(span=> unsafe{NonZeroU8::new_unchecked(#v)}).into()
//    }
//}
