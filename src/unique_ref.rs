use core::mem::{self, forget, ManuallyDrop};
use core::ops::{Deref, DerefMut};
use core::pin::Pin;
use core::ptr;

use crate::Memory;
use crate::MovingTo;

#[repr(transparent)]
/// A reference, similar to Pin but
/// that signify exclusive access
///
/// It is used by types that needs exclusive access to the
/// data, though the data cannot be moved as it is pinned in
/// memory.
pub struct UniqueRef<T>(Pin<T>);

impl<T: Deref> UniqueRef<T> {
    /// # Safety
    /// The pointee should always be accessed through this
    /// unique ref for the lifetime of this unique ref.
    #[inline(always)]
    pub unsafe fn new_unchecked(v: T) -> Self {
        Self(Pin::new_unchecked(v))
    }
    #[inline(always)]
    pub unsafe fn from_pin_unchecked(v: Pin<T>) -> Self {
        Self(v)
    }
    /// Transform into the underlying Pinned reference
    ///
    /// # Safety
    ///
    /// This function is unsafe because not dropping
    /// the value is likely to break expected invariants.
    #[inline(always)]
    pub unsafe fn into_pin(self) -> Pin<T> {
        self.0
    }
    /// Transform the current reference into
    /// another one
    #[inline(always)]
    pub unsafe fn map_unchecked<F, U>(self, f: F) -> UniqueRef<U>
    where
        F: FnOnce(T) -> U,
        U: Deref,
    {
        UniqueRef::new_unchecked(f(Pin::into_inner_unchecked(self.into_pin())))
    }
}

impl<'a, T: Deref> Deref for UniqueRef<T> {
    type Target = Pin<T>;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl<'a, T: DerefMut> DerefMut for UniqueRef<T> {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

/// As UniqueRef but drops the target value when dropper
///
/// Hold a reference to a value of type T that is
/// dropped when the OwnedRef is dropped
#[repr(transparent)]
pub struct OwnedRef<T: Deref + DerefMut>(UniqueRef<T>);

impl<T: Deref + DerefMut> OwnedRef<T> {
    /// Returns an UniqueRef to v.
    ///
    /// # Safety
    ///
    /// The referant shall only be refered by the
    /// the given pointer. Any attempt to access
    /// the data through a pass that does not involves
    /// the constructed UniqueRef is likely to cause
    /// undefined behavior.
    ///
    /// UniqueRef modelize to concept of exclusive owner ship,
    /// through a reference.
    ///
    /// It is used by types that needs exclusive access to the
    /// data, though the data cannot be moved as it is pinned in
    /// memory.
    #[inline(always)]
    pub unsafe fn new_unchecked(v: T) -> Self {
        Self(UniqueRef::new_unchecked(v))
    }
    /// Transform into the underlying Pinned reference
    ///
    /// # Safety
    ///
    /// This function is unsafe because not dropping
    /// the value is likely to break expected invariants.
    #[inline(always)]
    pub unsafe fn into_unique(self) -> UniqueRef<T> {
        let mut this = ManuallyDrop::new(self);
        ptr::read(&mut this.0)
    }
    /// Transform into the underlying Pinned reference
    ///
    /// # Safety
    ///
    /// This function is unsafe because not dropping
    /// the value is likely to break expected invariants.
    #[inline(always)]
    pub unsafe fn into_pin(self) -> Pin<T> {
        self.into_unique().into_pin()
    }
    /// Transform the current owned reference into
    /// another one
    #[inline(always)]
    pub unsafe fn map_unchecked<F, U>(self, f: F) -> OwnedRef<U>
    where
        F: FnOnce(T) -> U,
        U: Deref+DerefMut,
    {
        OwnedRef::new_unchecked(f(Pin::into_inner_unchecked(self.into_pin())))
    }
}

impl<T: Deref + DerefMut> OwnedRef<T>
where
    <T as Deref>::Target: Sized + MovingTo,
{
    #[inline(always)]
    pub unsafe fn move_to_unchecked(
        mut self,
        mem: &mut Memory,
    ) -> OwnedRef<&'_ mut <T as Deref>::Target> {
        self.as_mut().get_unchecked_mut().moving_to(mem);
        forget(self);
        OwnedRef::new_unchecked(mem.as_unchecked_mut::<<T as Deref>::Target>())
    }
    #[inline(always)]
    pub fn move_to(self, mem: &mut Memory) -> OwnedRef<&'_ mut <T as Deref>::Target> {
        assert_eq!(mem::size_of::<T>(), mem.len());
        assert!(mem::align_of::<T>() <= mem.alignment());
        unsafe { self.move_to_unchecked(mem) }
    }
}
impl<'a, T: Deref + DerefMut> OwnedRef<&'a mut [T]>
where
    [T]: MovingTo,
{
    #[inline(always)]
    pub unsafe fn move_slice_to_unchecked(mut self, mem: &mut Memory) -> OwnedRef<&'_ mut [T]> {
        self.as_mut().get_unchecked_mut().moving_to(mem);
        let l = self.len();
        forget(self);
        OwnedRef::new_unchecked(mem.as_unchecked_slice_mut::<T>(l))
    }
    #[inline(always)]
    pub fn move_slice_to(self, mem: &mut Memory) -> OwnedRef<&'_ mut [T]> {
        assert_eq!(mem::size_of::<T>(), mem.len());
        assert!(mem::align_of::<T>() <= mem.alignment());
        unsafe { self.move_slice_to_unchecked(mem) }
    }
}

impl<T> Drop for OwnedRef<T>
where
    T: Deref + DerefMut,
{
    #[inline(always)]
    fn drop(&mut self) {
        unsafe { core::ptr::drop_in_place(self.as_mut().get_unchecked_mut()) }
    }
}
impl<T: Deref + DerefMut> Deref for OwnedRef<T> {
    type Target = UniqueRef<T>;
    #[inline(always)]
    fn deref(&self) -> &UniqueRef<T> {
        &self.0
    }
}
impl<T: Deref + DerefMut> DerefMut for OwnedRef<T> {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
