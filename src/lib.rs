#![cfg_attr(not(test), no_std)]
#![cfg_attr(
    feature = "nightly",
    feature(min_const_generics),
    feature(const_fn),
    feature(const_raw_ptr_deref),
    feature(const_mut_refs),
    feature(const_ptr_offset),
    feature(const_ptr_offset_from),
    feature(const_size_of_val),
)]

//! Structures and traits to represent and safely manipulate any data as raw memory
//!
//!
//! # Examples
//!
//! Any kind of data can be viewed as constant memory slice:
//! ```
//! use memory_slice::AsMemory;
//! let v: [u8;4] = [1,1,1,1];
//! //as_memory return a &memory_slice
//! assert_eq!(unsafe{v.as_memory().read::<i32>()},1 + (1<<8) + (1<<16) + (1<<24));
//! ```
//!
//! But only types that does not preserve any invariants are accessible as mutable memory slice:
//!
//! This will compile:
//! ```
//! use memory_slice::AsMutMemory;
//! let mut v: [u8;2] = [1,1];
//! //as_memory return a &memory_slice
//! v.as_mut_memory().write(16 as u16);
//! ```
//!
//! This will not compile:
//! ```compile_fail
//! use memory_slice::AsMutMemory;
//! use std::string::String;
//! let mut v = String::new();
//! //as_memory return a &memory_slice
//! v.as_mut_memory().write(16 as u16);
//! ```
//!
//! Mutable memory slices can be used to write information of any type
//! while preserving borrow rules. The API provide also a smart pointer
//! that will drop value created on the memory slice:
//! ```
//! use memory_slice::{align,AsMutMemory,AsMemory,layout_for};
//! // creates an array of 64 u8 aligned as 8:
//! let mut buff = align!(8,[0 as u8;64]);
//!
//! //the create an int inside the buffer and get a reference to it
//! let (padding, v1, remaining_buffer) = buff.as_mut_memory().split_for_mut(layout_for::<i32>());
//! assert!(padding.is_empty());
//! let v1 = v1.write(42 as i32);
//! //unsafe{buff[0]}; //error => cannot borrow buff as immutable
//!
//! //use the remaining unitialized buffer to write an u64 in it:
//! let (padding, v2, remaining_buffer2) = remaining_buffer.split_for_mut(layout_for::<u64>());
//! assert_eq!(padding.len(), 4);
//! let v2 = v2.write(42 as u64);
//! //unsafe{remaing_buffer.read::<u8>()}; //error => cannot borrow remaining_buffer
//!
//! //v1 and v2 are reference to the i32 and u64 created inside buff
//! assert_eq!(*v1 as u64, *v2);
//!
//! {
//!     extern crate alloc;
//!     use alloc::borrow::ToOwned;
//!
//!     //In what remains of the buffer, let's create a value that needs to be dropped:
//!     let (_padding, v3, _remaining) = remaining_buffer2.split_for_mut(layout_for::<String>());
//!     let v3 = v3.emplace("42".to_owned());
//!
//!     //v3 is a smart pointer to the String created in the buffer that will drop
//!     //this string when it goes out of scope
//!     assert_eq!(*v1, v3.parse::<i32>().unwrap());
//! } //the string referenced by v3 is dropped
//!
//! //buff is not anymore borrowed, so it is accessible:
//! assert_eq!(unsafe { buff.as_memory().read_unchecked::<i32>() }, 42);
//!
//! //memory slice can be indexed (!!less inoffensive than it looks)
//! unsafe{*buff.as_mut_memory()[2..4].as_mut()=16 as u16};
//! assert_ne!(unsafe { buff.as_memory().read_unchecked::<i32>() }, 42);
//! ```
//!
//! A macro named `buffer` is provided to create un initialized
//! buffer:
//! ```
//! use memory_slice::buffer;
//! // create an uninitialized buffer of 64 bytes aligned as 8.
//! let mut buff = buffer!(64,8);
//! // buffer are dereferencable as memory_slice
//!
//! //then create an int inside the buffer
//! unsafe{buff.write_unchecked(42 as i32)};
//! ```
//!
mod default_at;
pub use default_at::{DefaultAt,MovingTo};

mod unique_ref;
pub use unique_ref::{UniqueRef,OwnedRef};

extern crate memory_slice_macro;
pub use memory_slice_macro::DefaultAt;

extern crate memoffset;
pub use memoffset::{span_of,offset_of};

extern crate contracts;
use contracts::*;

use const_fn::const_fn;

use core::alloc::Layout;
use core::marker::{Unpin};
use core::mem::{self, MaybeUninit};
use core::slice::SliceIndex;

use core::fmt::{self, Debug};
use core::ops::{Deref, DerefMut, Index, IndexMut, RangeBounds};
use core::pin::Pin;
use core::slice;

type Underlying = u8;
type MemLocation = u8;

/// Represents a raw memory range
///
/// References to this types are used to alias any kind
/// of objects that can be used as a memory buffer.
#[repr(transparent)]
pub struct Memory {
    inner: [Underlying],
}

/// Enable conversion of any type to a constant memory slice
///
/// This trait is implemented for every sized type and slices
pub trait AsMemory {
    #[inline(always)]
    fn as_memory(&self) -> &Memory {
        unsafe {
            Memory::from_raw_parts(
                self as *const _ as *const MemLocation,
                mem::size_of_val(self),
            )
        }
    }
}

/// Enable conversion to a mutable memory.
///
/// This trait should only be implemented for types that
/// does not maintain any internal invariants.
///
/// It is implemented for integer types and for all
/// slice \[T\] where T implements this trait.
pub trait AsMutMemory: AsMemory {
    #[inline(always)]
    fn as_mut_memory(&mut self) -> &mut Memory {
        unsafe {
            Memory::from_raw_parts_mut(self as *mut _ as *mut MemLocation, mem::size_of_val(self))
        }
    }
}

impl<T: ?Sized> AsMemory for T {}

impl<T> AsMutMemory for [T] where T: AsMutMemory {}

#[cfg(feature = "nightly")]
impl<T, const N: usize> AsMutMemory for [T; N] where T: AsMutMemory {}

impl AsMutMemory for Memory {}

impl<T> AsMutMemory for MaybeUninit<T> {}

macro_rules! declare_trait {
    ($trait:ident ,$($type: ty),+) =>  ($(
        impl $trait for $type { }
        )*)
}

declare_trait! {AsMutMemory, u8, u16, u32, u64, u128, i8, i16, i32, i64, i128, isize, usize, f32, f64}

pub const fn layout_for<T>() -> Layout {
    unsafe { Layout::from_size_align_unchecked(mem::size_of::<T>(), mem::align_of::<T>()) }
}

pub trait IndexConvertionTrait {
    type ConvOutput: ?Sized;
    fn do_convertion(&self) -> &Self::ConvOutput;
    fn do_convertion_mut(&mut self) -> &mut Self::ConvOutput;
}
impl IndexConvertionTrait for Underlying {
    type ConvOutput = Underlying;
    #[inline(always)]
    fn do_convertion(&self) -> &Self::ConvOutput {
        self
    }
    #[inline(always)]
    fn do_convertion_mut(&mut self) -> &mut Self::ConvOutput {
        self
    }
}
impl IndexConvertionTrait for [Underlying] {
    type ConvOutput = Memory;
    #[inline(always)]
    fn do_convertion(&self) -> &Self::ConvOutput {
        self.as_memory()
    }
    #[inline(always)]
    fn do_convertion_mut(&mut self) -> &mut Self::ConvOutput {
        self.as_mut_memory()
    }
}
pub use mem::size_of;

#[macro_export]
macro_rules! memory_slice_over {
    ($v:expr, $T:ty) => {
        unsafe {
            &*(&*($v as *const $T as *const [u8; $crate::size_of::<$T>()]) as &[u8] as *const [u8]
                as *const $crate::Memory)
        }
    };
}
#[macro_export]
macro_rules! memory_slice_over_mut {
    ($v:expr, $T:ty) => {
        unsafe {
            &mut *(&mut *($v as *mut $T as *mut [u8; $crate::size_of::<$T>()]) as &mut [u8]
                as *mut [u8] as *mut $crate::Memory)
        }
    };
}

impl Memory {
    #[test_ensures(loc == ret as *const _ as *const MemLocation)]
    /// Returns a constant memory slice of lenght `len` located at `loc`
    ///
    /// # Safety
    ///
    /// The referenced memory shall be a contiguous readable memory
    #[inline(always)]
    pub unsafe fn from_raw_parts<'a>(loc: *const MemLocation, len: usize) -> &'a Self {
        //mem::transmute(slice::from_raw_parts(loc as *const Underlying, len));
        &*(slice::from_raw_parts(loc as *const Underlying, len) as *const [Underlying]
            as *const Self)
    }

    #[test_ensures(loc == ret as *mut _ as *mut MemLocation)]
    /// Returns a mutable memory slice of lenght `len` located at `loc`
    ///
    /// # Safety
    ///
    /// The referenced memory shall be a contiguous readable and writable memory
    #[inline(always)]
    pub unsafe fn from_raw_parts_mut<'a>(loc: *mut MemLocation, len: usize) -> &'a mut Self {
        &mut *(slice::from_raw_parts_mut(loc as *mut Underlying, len) as *mut [Underlying]
            as *mut Self)
    }

    #[cfg_attr(not(feature="nightly"),test_ensures(ret == self as *const _ as *const MemLocation))]
    /// Returns a pointer to the first byte of memory
    #[inline(always)]
    pub const fn as_ptr(&self) -> *const MemLocation {
        self.inner.as_ptr() as *const MemLocation
    }

    #[const_fn(feature = "nightly")]
    #[cfg_attr(not(feature="nightly"),test_ensures(ret == self as *mut _ as *mut MemLocation))]
    /// Returns a mutable pointer to the first byte of memory
    #[inline(always)]
    pub const fn as_mut_ptr(&mut self) -> *mut MemLocation {
        self.inner.as_mut_ptr() as *mut MemLocation
    }

    /// Returns a sub memory slice
    #[inline(always)]
    pub unsafe fn get_unchecked<'a, I>(
        &'a self,
        index: I,
    ) -> &<<I as SliceIndex<[Underlying]>>::Output as IndexConvertionTrait>::ConvOutput
    where
        I: SliceIndex<[Underlying]>,
        <I as SliceIndex<[Underlying]>>::Output: IndexConvertionTrait + 'a,
    {
        self.inner.get_unchecked(index).do_convertion()
    }

    /// Returns a sub memory slice
    #[inline(always)]
    pub unsafe fn get_unchecked_mut<'a, I>(
        &'a mut self,
        index: I,
    ) -> &mut <<I as SliceIndex<[Underlying]>>::Output as IndexConvertionTrait>::ConvOutput
    where
        I: SliceIndex<[Underlying]>,
        <I as SliceIndex<[Underlying]>>::Output: IndexConvertionTrait + 'a,
    {
        self.inner.get_unchecked_mut(index).do_convertion_mut()
    }
    /// Returns a sub memory slice
    #[inline(always)]
    pub fn get<'a, I>(
        &'a self,
        index: I,
    ) -> Option<&<<I as SliceIndex<[Underlying]>>::Output as IndexConvertionTrait>::ConvOutput>
    where
        I: SliceIndex<[Underlying]>,
        <I as SliceIndex<[Underlying]>>::Output: IndexConvertionTrait + 'a,
    {
        self.inner.get(index).map(|x| x.do_convertion())
    }

    /// Returns a sub memory slice
    #[inline(always)]
    pub fn get_mut<'a, I>(
        &'a mut self,
        index: I,
    ) -> Option<&mut <<I as SliceIndex<[Underlying]>>::Output as IndexConvertionTrait>::ConvOutput>
    where
        I: SliceIndex<[Underlying]>,
        <I as SliceIndex<[Underlying]>>::Output: IndexConvertionTrait + 'a,
    {
        self.inner.get_mut(index).map(|x| x.do_convertion_mut())
    }

    #[test_ensures(ret == mem::size_of_val(self))]
    #[inline(always)]
    /// Returns the memory size.
    pub fn len(&self) -> usize {
        self.inner.len()
    }

    #[test_ensures(ret == mem::size_of_val(self))]
    #[inline(always)]
    /// Returns the memory size.
    pub fn size(&self) -> usize {
        self.inner.len()
    }

    #[test_ensures(ret -> self.len()==0)]
    /// Returns true is length is null.
    #[inline(always)]
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    #[test_ensures(self.as_ptr() as usize % ret == 0)]
    #[test_ensures(self.as_ptr() as usize % (2*ret) == ret)]
    /// Returns the alignment of the memory slice
    #[inline(always)]
    pub fn alignment(&self) -> usize {
        1 << (self.as_ptr() as usize).trailing_zeros()
    }

    #[test_ensures(self.len() == ret.0.len() + ret.1.len())]
    #[test_ensures(ret.0.len() == mid)]
    #[test_ensures(self.as_ptr() == ret.0.as_ptr())]
    #[test_ensures(ret.0.as_ptr().add(mid) == ret.1.as_ptr())]
    /// Split the memory slice at `mid`
    ///
    /// # Safety
    /// `mid` must be equal or smaller to memory len
    #[inline(always)]
    pub unsafe fn split_at_unchecked(&self, mid: usize) -> (&Self, &Self) {
        let l = self.len();
        let ptr = self.as_ptr();
        (
            Memory::from_raw_parts(ptr, mid),
            Memory::from_raw_parts(ptr.add(mid), l - mid),
        )
    }

    #[test_ensures(self.len() == ret.0.len() + ret.1.len())]
    #[test_ensures(ret.0.len() == mid)]
    #[test_ensures(self.as_ptr() == ret.0.as_ptr())]
    #[test_ensures(ret.0.as_ptr().add(mid) == ret.1.as_ptr())]
    /// Mutably split the memory slice at `mid`
    #[inline(always)]
    pub unsafe fn split_at_unchecked_mut<'a>(&mut self, mid: usize) -> (&mut Self, &mut Self) {
        let l = self.len();
        let ptr = self.as_mut_ptr();
        (
            Memory::from_raw_parts_mut(ptr, mid),
            Memory::from_raw_parts_mut(ptr.add(mid), l - mid),
        )
    }

    /// Split the memory slice at `mid`
    #[inline(always)]
    pub fn split_at(&self, mid: usize) -> (&Self, &Self) {
        assert!(mid <= self.len());
        unsafe { self.split_at_unchecked(mid) }
    }

    /// Mutably split the memory slice at `mid`
    #[inline(always)]
    pub fn split_at_mut<'a>(&mut self, mid: usize) -> (&mut Self, &mut Self) {
        assert!(mid <= self.len());
        unsafe { self.split_at_unchecked_mut(mid) }
    }

    #[test_ensures(unsafe{*(&self.inner as *const [Underlying] as *const[u8]) == *(&other.inner as *const [Underlying] as *const[u8])})]
    /// Copy the content of a an other memory slice into self
    #[inline(always)]
    pub fn copy_from_memory(&mut self, other: &Self) {
        self.inner.copy_from_slice(unsafe {
            slice::from_raw_parts(other.as_ptr() as *const Underlying, other.len())
        });
    }

    /// Move data within this memory slice
    #[inline(always)]
    pub fn copy_within<R: RangeBounds<usize>>(&mut self, range: R, dest: usize) {
        self.inner.copy_within(range, dest);
    }

    #[debug_requires(self.alignment()>=mem::align_of::<T>())]
    #[debug_requires(self.len()>=mem::size_of::<T>())]
    #[test_ensures(self.as_ptr() == ret as *const _ as *const MemLocation)]
    /// Returns a reference of type &T that points to the first memory location
    ///
    /// # Safety
    ///
    /// The following pre-condition shall be filled:
    ///   - The memory is at least as aligned as T
    ///   - The memory size is at least as large as T
    ///   - The memory is a valid value representation of type T
    #[inline(always)]
    pub unsafe fn as_unchecked_ref<T>(&self) -> &T {
        &*(self.as_ptr() as *const T)
    }

    #[debug_requires(self.alignment()>=mem::align_of::<T>())]
    #[debug_requires(self.len()>=mem::size_of::<T>())]
    #[test_ensures(self.as_ptr() == ret as *mut _ as *const MemLocation)]
    /// Returns a mutable reference of type &mut T that points to the first memory location
    ///
    /// # Safety
    ///
    /// The following pre-condition shall be filled:
    ///   - The memory is at least as aligned as T
    ///   - The memory size is at least as large as T
    ///   - The memory is a valid value representation of type T
    #[inline(always)]
    pub unsafe fn as_unchecked_mut<T>(&mut self) -> &mut T {
        &mut *(self.as_mut_ptr() as *mut T)
    }

    #[debug_requires(self.alignment()>=mem::align_of::<T>())]
    #[debug_requires(self.len()>=len*mem::size_of::<T>())]
    #[test_ensures(self.as_ptr() == ret.as_ptr() as *const MemLocation)]
    /// Returns a slice of type &\[T\] that points to the first memory location
    ///
    /// # Safety
    ///
    /// The following pre-condition shall be filled:
    ///   - The memory is at least as aligned as T
    ///   - The memory size is at least as large as `len * size_of::<T>::()`
    ///   - The memory is a valid value representation of type T
    #[inline(always)]
    pub unsafe fn as_unchecked_slice<T>(&self, len: usize) -> &[T] {
        let ptr = self.as_ptr() as *const T;
        slice::from_raw_parts(ptr, len)
    }

    #[debug_requires(self.alignment()>=mem::align_of::<T>())]
    #[debug_requires(self.len()>=len*mem::size_of::<T>())]
    #[test_ensures(self.as_ptr() == ret.as_ptr() as *const MemLocation)]
    /// Returns a mutable slice of type &mut \[T\] that points to the first memory location
    ///
    /// # Safety
    ///
    /// The following pre-condition shall be filled:
    ///   - The memory is at least as aligned as T
    ///   - The memory size is at least as large as `len * size_of::<T>::()`
    ///   - The memory is a valid value representation of type T
    #[inline(always)]
    pub unsafe fn as_unchecked_slice_mut<T>(&mut self, len: usize) -> &mut [T] {
        let ptr = self.as_mut_ptr() as *mut T;
        slice::from_raw_parts_mut(ptr, len)
    }

    /// Returns a reference of type &T that points to the first memory location
    ///
    /// # Panic
    ///
    /// The following pre-condition will cause a panic:
    ///   - The memory is at least as aligned as T
    ///   - The memory size equals T size.
    ///
    /// # Safety
    /// The following pre-condition will cause a panic:
    ///   - The memory is a valid value representation of type T
    #[inline(always)]
    pub unsafe fn as_ref<T>(&self) -> &T {
        assert_eq!(self.size(), mem::size_of::<T>());
        assert!(self.alignment() >= mem::align_of::<T>());
        self.as_unchecked_ref()
    }

    /// Returns a mutable reference of type &mut T that points to the first memory location
    ///
    /// # Panic
    ///
    /// The following pre-condition will cause a panic:
    ///   - The memory is at least as aligned as T
    ///   - The memory size equals T size.
    ///
    /// # Safety
    /// The following pre-condition will cause a panic:
    ///   - The memory is a valid value representation of type T
    #[inline(always)]
    pub unsafe fn as_mut<T>(&mut self) -> Pin<&mut T> {
        assert_eq!(self.size(), mem::size_of::<T>());
        assert!(self.alignment() >= mem::align_of::<T>());
        Pin::new_unchecked(self.as_unchecked_mut())
    }

    /// Returns a slice of type &\[T\] that points to the first memory location
    ///
    /// # Panic
    ///
    /// The following pre-condition will cause a panic:
    ///   - The memory is at least as aligned as T
    ///   - The memory size equals n times T size.
    ///
    /// # Safety
    /// The following pre-condition will cause a panic:
    ///   - The memory is a valid value representation of type T
    #[inline(always)]
    pub unsafe fn as_slice<T>(&self, len: usize) -> &[T] {
        assert_eq!(self.size(), len * mem::size_of::<T>());
        assert!(self.alignment() >= mem::align_of::<T>());
        self.as_unchecked_slice(len)
    }

    /// Returns a mutable slice of type &mut \[T\] that points to the first memory location
    ///
    /// # Panic
    ///
    /// The following pre-condition will cause a panic:
    ///   - The memory is at least as aligned as T
    ///   - The memory size equals n times T size.
    ///
    /// # Safety
    /// The following pre-condition will cause a panic:
    ///   - The memory is a valid value representation of type T
    #[track_caller]
    #[inline(always)]
    pub unsafe fn as_slice_mut<T>(&mut self, len: usize) -> Pin<&mut [T]> {
        assert_eq!(self.size(), len * mem::size_of::<T>());
        assert!(self.alignment() >= mem::align_of::<T>());
        Pin::new_unchecked(self.as_unchecked_slice_mut(len))
    }

    #[test_ensures(ret.0.len()<layout.align())]
    #[test_ensures(ret.1.len()==layout.size())]
    #[test_ensures(ret.1.alignment()>=layout.align())]
    #[test_ensures((self.alignment()<layout.align()) -> ret.1.alignment() == layout.align())]
    #[test_ensures((self.alignment()==layout.align()) -> (ret.0.len() == 0))]
    #[test_ensures(ret.0.len()+ret.1.len()+ret.2.len() == self.len())]
    #[test_ensures(ret.0.as_ptr() == self.as_ptr())]
    #[test_ensures(unsafe{ret.0.as_ptr().add(ret.0.len())} == ret.1.as_ptr())]
    #[test_ensures(unsafe{ret.1.as_ptr().add(ret.1.len())} == ret.2.as_ptr())]
    /// Create a split of the memory for the given layout
    ///
    /// Returns 3 memory slice:
    ///   - the first slice represent the alignment padding
    ///   - the second a memory slice sweatable for layout
    ///   - the third is the reaming memory
    ///
    #[inline(always)]
    pub fn split_for(&self, layout: Layout) -> (&Self, &Self, &Self) {
        let s0 = self.as_ptr().align_offset(layout.align());
        let (pad, rest) = self.split_at(s0);
        let (data, rem) = rest.split_at(layout.size());
        (pad, data, rem)
    }

    #[test_ensures(ret.0.len()<layout.align())]
    #[test_ensures(ret.1.len()==layout.size())]
    #[test_ensures(ret.1.alignment()>=layout.align())]
    #[test_ensures((self.alignment()==layout.align()) -> (ret.0.len() == 0))]
    #[test_ensures(ret.0.len()+ret.1.len()+ret.2.len() == self.len())]
    #[test_ensures(ret.0.as_ptr() == self.as_ptr())]
    #[test_ensures(unsafe{ret.0.as_ptr().add(ret.0.len())} == ret.1.as_ptr())]
    #[test_ensures(unsafe{ret.1.as_ptr().add(ret.1.len())} == ret.2.as_ptr())]
    /// Create a split of the memory for the given layout
    ///
    /// Returns 3 memory slice:
    ///   - the first slice represent the alignment padding
    ///   - the second a memory slice sweatable for layout
    ///   - the third is the reaming memory
    ///
    #[inline(always)]
    pub fn split_for_mut(&mut self, layout: Layout) -> (&mut Self, &mut Self, &mut Self) {
        let l0 = self.as_ptr().align_offset(layout.align());
        let l1 = layout.size();
        let l = self.len();
        let l01 = l0 + l1;
        assert!(l01 <= l);
        let ptr = self.as_mut_ptr();
        unsafe {
            (
                Memory::from_raw_parts_mut(ptr, l0),
                Memory::from_raw_parts_mut(ptr.add(l0), l1),
                Memory::from_raw_parts_mut(ptr.add(l01), l - l01),
            )
        }
    }


    /// Returns an OwnedRef that refers to a &mut T that points to the first memory location
    ///
    /// # Safety
    ///
    /// The following pre-condition shall be filled:
    ///   - The memory is a valid value representation of type T
    ///
    /// # Panics
    ///
    /// This function will panic if the memory slice is not large enough for T and needed alignment
    /// padding.
    ///
    #[inline(always)]
    pub unsafe fn as_owned_ref<T>(&mut self) -> OwnedRef<&'_ mut T> {
        assert_eq!(self.size(), mem::size_of::<T>());
        assert!(self.alignment() >= mem::align_of::<T>());
        let v = self.as_unchecked_mut();
        OwnedRef::new_unchecked(v)
    }

    /// Returns an OwnedRef to a mutable slice of type &mut \[T\] that points to the first memory location
    ///
    /// # Safety
    ///
    /// The following pre-condition shall be filled:
    ///   - The memory is a valid value representation of type T
    ///
    /// # Panics
    ///
    /// This function will panic if the memory slice is not large enough for `n` `T` and needed alignment
    /// padding.
    #[inline(always)]
    pub unsafe fn get_owned_slice<T>(&mut self, n: usize) -> OwnedRef<&'_ mut [T]> {
        assert_eq!(self.size(), n * mem::size_of::<T>());
        assert!(self.alignment() >= mem::align_of::<T>());
        let v = self.as_unchecked_slice_mut::<T>(n);
        OwnedRef::new_unchecked(v)
    }

    /// Writes a value of type T by consuming it and
    /// returns a reference to it
    ///
    ///
    /// # Safety
    ///
    /// The following pre-condition shall be filled:
    ///   - T may not implement Unpin, in which case the value of val
    ///     should fullfill the invariants of T assuming it is located
    ///     in the memory referenced by this memory slice.
    ///   - The memory is at least as aligned as T
    ///   - The memory size is at least as large as T
    #[inline(always)]
    pub unsafe fn write_unchecked<T>(&mut self, val: T) {
        core::ptr::copy_nonoverlapping(&val, self.as_mut_ptr() as *mut T, 1);
        core::mem::forget(val);
    }

    /// Writes a value of type T by consuming it and
    /// returns a reference to it
    ///
    /// # Panics
    ///
    /// This function will panic if the memory slice is not large enough for T and needed alignment
    /// padding.
    ///
    #[inline(always)]
    pub fn write<T>(&mut self, val: T) -> Pin<&mut T> {
        assert_eq!(self.size(), mem::size_of::<T>());
        assert!(self.alignment() >= mem::align_of::<T>());
        unsafe {
            self.write_unchecked(val);
            Pin::new_unchecked(self.as_unchecked_mut())
        }
    }

    /// Writes a value of type T by consuming it and
    /// returns an owned reference to it
    ///
    /// # Panic
    ///
    /// The following pre-condition shall be filled:
    ///   - The memory is at least as aligned as T;
    ///   - The memory size is different from T size.
    #[inline(always)]
    pub fn emplace<T>(&mut self, val: T) -> OwnedRef<&'_ mut T> {
        assert!(self.alignment() >= mem::align_of::<T>());
        assert_eq!(self.len(), mem::size_of::<T>());
        unsafe {
            self.write_unchecked(val);
            OwnedRef::new_unchecked(self.as_unchecked_mut())
        }
    }

    /// Reads a value of type T
    ///
    /// # Safety
    ///
    /// The following pre-conditions shall be fullfilled:
    ///   - The memory size is as large as T
    ///   - The memory has a valid value representation of type T
    #[inline(always)]
    pub unsafe fn read_unchecked<T>(&self) -> T {
        let mut b = MaybeUninit::uninit();
        core::ptr::copy_nonoverlapping(self.as_ptr() as *const T, b.as_mut_ptr(), 1);
        b.assume_init()
    }

    /// Reads a value of type T
    ///
    /// # Panic
    ///
    /// The following pre-conditions shall be fullfilled:
    ///   - The memory size is as large as T
    ///   - The memory is at least as aligned as T
    ///
    /// # Safety
    ///
    /// The following pre-conditions shall be fullfilled:
    ///   - The memory has a valid value representation of type T
    #[inline(always)]
    pub unsafe fn read<T: Unpin>(&self) -> T {
        assert!(self.alignment() >= mem::align_of::<T>());
        assert_eq!(self.len(), mem::size_of::<T>());
        self.read_unchecked()
    }

}
impl Debug for Memory {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_fmt(format_args!(
            "memory_slice::Memory(location: {:?}, length: {:?})",
            self.as_ptr(),
            self.len()
        ))
    }
}

impl<I> Index<I> for Memory
where
    I: SliceIndex<[Underlying]>,
    <I as SliceIndex<[Underlying]>>::Output: IndexConvertionTrait + 'static,
{
    type Output = <<I as SliceIndex<[Underlying]>>::Output as IndexConvertionTrait>::ConvOutput;
    #[inline(always)]
    fn index(&self, index: I) -> &Self::Output {
        self.get(index).expect("Index out of bound")
    }
}
impl<I> IndexMut<I> for Memory
where
    I: SliceIndex<[Underlying]>,
    <I as SliceIndex<[Underlying]>>::Output: IndexConvertionTrait + 'static,
{
    #[inline(always)]
    fn index_mut(&mut self, index: I) -> &mut Self::Output {
        self.get_mut(index).expect("Index out of bound")
    }
}

#[repr(transparent)]
#[derive(Debug, Copy)]
/// A buffer of uninitialized memory with same size and alignement as T.
///
/// Those buffer implement `Deref<Target=Memory>` and `DerefMut<Target=Memory>`
/// so they have the same interface as [Memory].
pub struct BufferAs<T>(MaybeUninit<T>);

impl<T> BufferAs<T> {
    /// Create an uninitialized buffer.
    #[inline(always)]
    pub const fn new() -> Self {
        Self(MaybeUninit::uninit())
    }
    /// Create a zeroed buffer.
    #[inline(always)]
    pub fn zeroed() -> Self {
        Self(MaybeUninit::zeroed())
    }
}

impl<T> Deref for BufferAs<T> {
    type Target = Memory;
    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        unsafe {
            Memory::from_raw_parts(
                self as *const _ as *const MemLocation,
                mem::size_of_val(self),
            )
        }
    }
}

impl<T> DerefMut for BufferAs<T> {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe {
            Memory::from_raw_parts_mut(self as *mut _ as *mut MemLocation, mem::size_of_val(self))
        }
    }
}

impl<T> AsMutMemory for BufferAs<T> {}

impl<T> Clone for BufferAs<T> {
    #[inline(always)]
    fn clone(&self) -> Self {
        let mut r = MaybeUninit::<Self>::uninit();
        unsafe {
            core::ptr::copy_nonoverlapping(self, r.as_mut_ptr(), 1);
            r.assume_init()
        }
    }
}


#[macro_export]
/// Returns an overaligned value.
///
/// `align(alignment,expr)` returns the value of a structure declared with #[repr(align($alignment))]
/// attributes that dereferences the result of `expr`.
macro_rules! align {
    ($alignment:literal, $exp:expr) => {{
        #[repr(align($alignment))]
        struct Aligner<T>(T);
        impl<T> ::core::ops::Deref for Aligner<T> {
            type Target = T;
            #[inline(always)]
            fn deref(&self) -> &T {
                &self.0
            }
        }
        impl<T> ::core::ops::DerefMut for Aligner<T> {
            #[inline(always)]
            fn deref_mut(&mut self) -> &mut T {
                &mut self.0
            }
        }
        impl<T> ::core::convert::AsRef<T> for Aligner<T> {
            #[inline(always)]
            fn as_ref(&self) -> &T {
                &*self
            }
        }
        impl<T> ::core::convert::AsMut<T> for Aligner<T> {
            #[inline(always)]
            fn as_mut(&mut self) -> &mut T {
                &mut *self
            }
        }
        impl<T> $crate::AsMutMemory for Aligner<T> where T: $crate::AsMutMemory {}
        Aligner($exp)
    }};
}

#[macro_export]
/// Creates a statically sized, uninitialized buffer on the stack.
///
/// `buffer(size)` returns a buffer of size `size` aligned as `16`.
///
/// `buffer(size,alignment)` returns a buffer of size `size` aligned as `alignment`.
///
///  The argument `size` must be a constant expression and `alignement` a litteral integer.
macro_rules! buffer {
    ($size:expr,$alignment:literal) => {{
        use $crate::align;
        align!($alignment, $crate::BufferAs::<[u8; $size]>::new())
    }};
    ($size:expr) => {{
        $crate::buffer!($size, 16)
    }};
}

#[macro_export]
/// Creates a statically sized, zeroed buffer on the stack.
///
/// `buffer(size)` returns a buffer of size `size` aligned as `16`.
///
/// `buffer(size,alignment)` returns a buffer of size `size` aligned as `alignment`.
///
///  The argument `size` must be a constant expression and `alignement` a litteral integer.
macro_rules! zeroed_buffer {
    ($size:expr,$alignment:literal) => {{
        use $crate::align;
        align!($alignment, $crate::BufferAs::<[u8; $size]>::zeroed())
    }};
    ($size:expr) => {{
        $crate::buffer!($size, 16)
    }};
}

#[cfg(test)]
mod tests {
    use super::buffer;
    use super::layout_for;
    use core::fmt::Debug;
    use core::mem;
    use rstest::*;

    #[rstest(
        value => [42 as usize,42 as u32,42 as u16, 42 as u8],
        mis_alignment => [0,1,2,3,4,5,6,7,8,9]
        )]
    fn write<T: Eq + Copy + Unpin + Debug>(value: T, mis_alignment: usize) {
        let mut buff = buffer!(64, 64);
        {
            let (_, buff) = buff.split_at_mut(mis_alignment);
            let (_, buff, _) = buff.split_for_mut(layout_for::<T>());
            let v = buff.write(value);
            assert_eq!(*v, value);
        }
        if mis_alignment == 0 {
            assert_eq!(unsafe { buff.read_unchecked::<T>() }, value);
            assert_eq!(*unsafe { buff.as_unchecked_ref::<T>() }, value);
        } else {
            let loc = ((mis_alignment - 1) / mem::size_of::<T>() + 1) * mem::size_of::<T>();
            println!("buff loc {:?}", buff.as_ptr());
            println!(
                "buff loc slice {:?}",
                buff[loc..loc + mem::size_of::<T>()].as_ptr()
            );
            println!("Location {}", loc);
            println!("Location end {}", loc + mem::size_of::<T>());
            assert_eq!(
                unsafe { buff[loc..loc + mem::size_of::<T>()].read::<T>() },
                value
            );
            println!("here");
            assert_eq!(*unsafe { buff[loc..].as_unchecked_ref::<T>() }, value);
        }
    }
    #[cfg(feature = "nightly")]
    mod nightly {
        use crate::{memory_slice_over, memory_slice_over_mut, Memory,BufferAs};
        struct X {
            x: *const X,
        }
        unsafe impl Sync for X {}
        impl X {
            const fn new_moving_to(v: &Memory) -> Self {
                X {
                    x: v.as_ptr() as *const X,
                }
            }
        }
        static X_STATIC: X = X::new_moving_to(memory_slice_over!(&X_STATIC, X));
        struct Y {
            y: *mut Y,
        }
        impl Y {
            const fn new_moving_to(v: &Memory) -> Self {
                Y {
                    y: v.as_ptr() as *mut Y,
                }
            }
            const fn new_moving_to_mut(v: &mut Memory) -> Self {
                Y {
                    y: v.as_mut_ptr() as *mut Y,
                }
            }
        }
        static mut Y_STATIC: Y = Y::new_moving_to(memory_slice_over!(&Y_STATIC, Y));

        #[test]
        fn memory_slice_over() {
            assert_eq!(X_STATIC.x, &X_STATIC as *const X);
            unsafe{assert_eq!(Y_STATIC.y, &mut Y_STATIC as *mut Y)};
            let mut y2 = BufferAs::<Y>::new();
            let y2_value = Y::new_moving_to_mut(memory_slice_over_mut!(&mut y2,BufferAs<Y>));
            let mut y2_ref = y2.write(y2_value);
            let loc = unsafe{y2_ref.as_mut().get_unchecked_mut()} as *mut Y;
            assert_eq!(y2_ref.y,loc);
        }

        
    }
    //struct X {
    //    x: *const X,
    //    b: *const u8,
    //}
    //impl X {
    //    const fn new_moving_to(auto: &X) -> X {
    //        let loc = auto as *const X as *const u8;
    //        X{x:auto, b: unsafe{loc.add(byte_offset(auto,&auto.b)) as *const u8}}
    //    }
    //}

    //unsafe impl Sync for X {}
    //#[repr(C)]
    //struct Y {
    //    a: X,
    //    b: X,
    //}
    //const fn offset() -> usize {
    //    let __ptr = core::ptr::NonNull::dangling().as_ptr() as *const X;
    //    unsafe{(&(*__ptr).x as *const _ as *const u8).offset_from(__ptr as *const u8) as usize}
    //}

    //const unsafe fn byte_offset<T,U>(from:&T, to:&U) -> usize {
    //    unsafe{(to as *const _ as * const u8).offset_from(from as *const _ as * const u8) as usize}
    //}
    //impl Y {
    //    const fn new_moving_to(auto: &Self) -> Self {
    //        Self {
    //        a: X::new_moving_to(&auto.a),
    //        b: X::new_moving_to(&auto.b),
    //        //b: X::new_moving_to(unsafe{&*(loc.add(Self::OFFSET_OF_B) as *const X)}),
    //        }
    //    }
    //}


    //static mut Y_STATIC: Y = unsafe{Y::new_moving_to(&Y_STATIC)};

}
