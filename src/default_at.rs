use super::{Memory,OwnedRef};
use core::pin::Pin;



/// Default constructibility on a given memory slice
pub trait DefaultAt: Sized {
    /// Default construct Self on the given memory slice
    /// 
    /// Returns a pinned mutable reference to self.
    ///
    /// # Panic
    ///
    /// The following condition will cause a panic:
    ///   - the memory slice size is not the same as Self
    ///   - the memory slice alignment is not greater or equal to the alignment of Self
    #[inline(always)]
    fn default_at(mem: &mut Memory) -> Pin<&mut Self> {
        mem.write(unsafe {Self::default_moving_to(mem)})
    }
    /// Default construct Self on the given memory slice
    ///
    /// Returns an [OwnedRef] to self.
    ///
    /// # Panic
    ///
    /// The following condition will cause a panic:
    ///   - the memory slice size is not the same as Self
    ///   - the memory slice alignment is not greater or equal to the alignment of Self
    #[inline(always)]
    fn emplace_at(mem: &mut Memory) -> OwnedRef<&mut Self> {
        mem.emplace(unsafe {Self::default_moving_to(mem)})
    }
    /// Default construct Self on the given memory slice
    ///
    /// # Safety
    ///
    /// The following condition may cause a undefined behavior:
    ///   - the memory slice size is smaller than Self 
    ///   - any memory location that could break Self inner invariants
    #[inline(always)]
    unsafe fn default_at_unchecked(mem: &mut Memory) {
        mem.write_unchecked(Self::default_moving_to(mem));
    }

    /// Construct Self with invariants set assuming it
    /// will soon be moved on the given memory slice
    ///
    /// # Safety
    ///
    /// This function is unsafe because its result is only
    /// intended to be moved in the given memory location. Any
    /// other action on the returned object may cause UB because
    /// of broken type invariants.
    unsafe fn default_moving_to(mem: &Memory) -> Self;
}
impl<T> DefaultAt for T
where
    T: Default,
{
    #[inline(always)]
    unsafe fn default_moving_to(_: &Memory) -> Self {
        Self::default()
    }
}

pub trait MovingTo {
    unsafe fn moving_to(&mut self, mem: &mut Memory);
}

#[macro_export]
macro_rules! sub_memory_slice_of {
    ($mem:expr, $t:ident, $field:tt) => (
        $mem.get_unchecked($crate::span_of!($t,$field))
        )
}

#[cfg(test)]
mod test {
    use super::{DefaultAt};
    use crate::{Memory,BufferAs};

struct X(*const X);

impl DefaultAt for X {
    unsafe fn default_moving_to(mem:&Memory) -> Self {
        Self(mem.as_ptr() as *const _)
    }
}

#[repr(C)]
struct Y(X,X);

impl DefaultAt for Y {
    unsafe fn default_moving_to(mem:&Memory) -> Self {
        Self(
            X::default_moving_to(sub_memory_slice_of!(mem,Y,0)),
            X::default_moving_to(sub_memory_slice_of!(mem,Y,1)),
            )
    }
}
#[test]
fn default_at() {
    let mut v = BufferAs::<X>::new();
    let a_x = X::default_at(&mut v);
    assert_eq!(a_x.as_ref().get_ref() as *const _, a_x.0);
    let mut v = BufferAs::<Y>::new();
    let a_y = Y::default_at(&mut v);
    assert_eq!(&a_y.0 as *const _, a_y.0.0);
    assert_eq!(&a_y.1 as *const _, a_y.1.0);
}

}
